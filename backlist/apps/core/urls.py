# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns('',
    url(r'^api/books/?$', BookViewSet.as_view({'get': 'list', 'post': 'create'})),

    url(r'^api/books/(?P<pk>\d+)/?$', BookViewSet.as_view(
        {'get': 'retrieve', 'delete': 'destroy', 'post': 'update'}),),

    url(r'^api/categories/?$', CategoriesListApi.as_view(), name='categories'),
)
