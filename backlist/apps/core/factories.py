# -*- coding: utf-8 -*-

import random
import factory
from factory.django import DjangoModelFactory, ImageField

from .models import *

AUTHOR_N = u'Лев Дмитрий Александр Пётр Алексей Jack Leo Ken'.split()
AUTHOR_S = u'Сергеевич Николаев Валерьевич Ярович Джонович Кенович'.split()
AUTHOR_L = u'Сидоренко Киров Будёнов Китов Rammone Dellone Russo'.split()

TYPES = [u'Приключения', u'Сказка о том как', u'Журнал -', u'Сборник стихов про']
NAMES = u'''война рыба мартышка облоко Печкин дочка отци котел
    самолет старик дом python manual лесоруб'''.split()
ACTION = u'''рубил купила сдала смастерил запрограмировал отдебажил бежал наблюдала рефакторил закончилась прыгала
    извергало горел летел'''.split()
PLUS = u'''медлено быстро непонятно ярко пламено неизмено окончательно высоко низко кашерно'''.split()
CATEGORIES = list(Category.objects.all())
ALL_CONST = [AUTHOR_L, AUTHOR_S, AUTHOR_N, TYPES, NAMES, ACTION, PLUS]


def get_authors():
    return list(Author.objects.all())


def random_element(seq):
    if seq:
        return seq[random.randint(0, len(seq)-1)]

rand_el = random_element


class BookFactory(DjangoModelFactory):
    FACTORY_FOR = Book

    image = ImageField(width=500, height=500, color=rand_el(['green', 'red', 'blue']))
    description = factory.LazyAttribute(lambda x: u' '.join([rand_el(rand_el(ALL_CONST))
                                                            for _ in range(random.randint(50, 120))]))

    @factory.lazy_attribute
    def title(self):
        return u' '.join([rand_el(TYPES), rand_el(NAMES), rand_el(ACTION), rand_el(PLUS)])

    @factory.lazy_attribute
    def category(self):
        return rand_el(CATEGORIES)

    @factory.post_generation
    def authors(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            self.authors.add(*extracted)
        else:
            authors = get_authors()
            self.authors.add(rand_el(authors), rand_el(authors))


class AuthorFactory(DjangoModelFactory):
    FACTORY_FOR = Author

    full_name = factory.LazyAttribute(lambda x: u' '.join([rand_el(AUTHOR_N), rand_el(AUTHOR_S), rand_el(AUTHOR_L)]))
