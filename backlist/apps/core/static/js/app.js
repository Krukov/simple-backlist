(function(){

    var app = angular.module('backlistApp', ['ngRoute', 'booksControllers']);

    app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common['X-CSRFToken'] = getCookie('csrftoken');
    }]);

    app.config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'static/templates/books_list.html',
            controller: 'BooksCtrl'
        }).when('/book/:id', {
            templateUrl: 'static/templates/edit_book.html',
            controller: 'BookEditCtrl'
        }).otherwise({
            redirectTo: '/'
        });
    }]);


    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);

                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
})();
