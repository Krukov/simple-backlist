(function() {

    var booksControllers = angular.module('booksControllers', ['ngResource']);

    booksControllers.factory('Book', ['$resource', function ($resource) {
        return $resource('/api/books/:id?format=json', {id: '@id'})
    }]);

    booksControllers.factory('Category', ['$resource', function ($resource) {
        return $resource('/api/categories/?format=json', {}, {
            'get': {'method': 'GET', isArray:true}
        })
    }]);

    booksControllers.controller('BooksCtrl', ['$scope', 'Book', function ($scope, Book) {
        $scope.books = Book.query();
        $scope.authors = [];

        $scope.delete = function (book) {
            Book.delete({'id': book.id});
            idx = $scope.books.indexOf(book)
            $scope.books.splice(idx, 1)
        };
    }]);

    booksControllers.controller('BookEditCtrl', [
        '$scope', '$routeParams', '$location', 'Book', 'Category',
            function ($scope, $routeParams, $location, Book, Category) {
                $scope.categories = Category.get();
                if ($routeParams.id == 'add') {
                    $scope.book = new Book();
                } else {
                    $scope.book = Book.get({id: $routeParams.id})
                }

                $scope.save = function() {
                    Book.save($scope.book).success(function(){
                        $location.path('/');
                    });
                };

                $scope.delete = function(book) {
                    Book.delete({'id': book.id}).success(function(){
                        $location.path('/');
                    });
                };
    }]);
})();
