# -*- coding: utf-8 -*-
from email.mime import image

from rest_framework.serializers import *

from .models import *


class CategorySimpleSer(ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'title']
        read_only_fields = ['title']


class AuthorSimpleSer(ModelSerializer):

    class Meta:
        model = Author
        fields = ['id', 'full_name']


class BookCompliteSer(ModelSerializer):
    category = CategorySimpleSer(required=False)
    authors = AuthorSimpleSer(many=True, required=False)
    image_thumb = CharField(source='thumb', required=False, read_only=True)

    class Meta:
        model = Book
        fields = ['id', 'title', 'description', 'category', 'authors', 'image_thumb']


class BookSimpleSer(ModelSerializer):

    class Meta:
        model = Book
        fields = ['id', 'title', 'description', 'category']
