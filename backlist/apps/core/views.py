# -*- coding: utf-8 -*-

from rest_framework.viewsets import ModelViewSet
from rest_framework import generics

from .serializers import *
from .models import *


class BookViewSet(ModelViewSet):
    serializer_class = BookCompliteSer
    queryset = Book.objects.all().select_related('category').prefetch_related('authors')

    def get_serializer_class(self):
        if self.request.method.lower() in ['post']:
            return BookSimpleSer
        return super(BookViewSet, self).get_serializer_class()


class CategoriesListApi(generics.ListAPIView):
    model = Category
    serializer_class = CategorySimpleSer
