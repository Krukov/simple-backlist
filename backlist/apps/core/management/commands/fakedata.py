# -*- coding: utf8 -*-
from django.core.management.base import BaseCommand
from django.db import transaction

from ...factories import *


class Command(BaseCommand):
    args = '[products]'
    help = 'Generates give number (1 by default) of faked books.'

    def handle(self, *args, **options):
        self.created_authors()
        with transaction.atomic():
            self.create_products(int(args[0]) if len(args) else 1)

    @staticmethod
    def create_products(count):
        print(u'Generate %s books' % count)
        [BookFactory() for _ in range(count)]
        print(u'End')

    @staticmethod
    def created_authors(count=5):
        print(u'Generate %s authors' % count)
        [AuthorFactory() for _ in range(count)]
