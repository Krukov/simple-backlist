# -*- coding: utf-8 -*-

from django.db import models

from easy_thumbnails.files import get_thumbnailer


class Category(models.Model):

    title = models.CharField(max_length=255, verbose_name=u'Название')
    parent = models.ForeignKey('self', null=True, blank=True, related_name='child', verbose_name=u'Родитель')

    class Meta:
        verbose_name, verbose_name_plural = u'Категория', u'Категории'

    def __unicode__(self):
        return self.title


class Book(models.Model):

    title = models.CharField(max_length=255, verbose_name=u'Название')
    image = models.ImageField(upload_to='books', null=True, blank=True, verbose_name=u'Обложка')
    description = models.TextField(max_length=255, null=True, blank=True, verbose_name=u'Описание')
    category = models.ForeignKey(Category, null=True, blank=True, verbose_name=u'Категория')

    class Meta:
        verbose_name, verbose_name_plural = u'Книга', u'Книги'

    def __unicode__(self):
        return self.title

    @property
    def thumb(self):
        return get_thumbnailer(self.image)['medium'].url if self.image else None


class Author(models.Model):

    full_name = models.CharField(max_length=255, verbose_name=u'ФИО')
    books = models.ManyToManyField(Book, null=True, blank=True, related_name='authors', verbose_name=u'Книги')

    class Meta:
        verbose_name, verbose_name_plural = u'Автор', u'Авторы'

    def __unicode__(self):
        return self.full_name
