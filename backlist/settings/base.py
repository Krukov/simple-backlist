import os

DEBUG = TEMPLATE_DEBUG = False
BASE_DIR = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', '..'))

TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-ru'
USE_I18N = True
USE_L10N = True
USE_TZ = True


ALLOWED_HOSTS = []


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',

    'south',
    'rest_framework',
    'easy_thumbnails',

    'backlist.apps.core',
)


SITE_ID = 1
ROOT_URLCONF = 'backlist.urls'
WSGI_APPLICATION = 'backlist.wsgi.application'

STATIC_URL = '/static/'
MEDIA_ROOT = os.path.normpath(os.path.join(BASE_DIR, 'media'))
MEDIA_URL = '/media/'


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'backlist', 'templates'),
)

THUMBNAIL_ALIASES = {
    '': {
        'medium': {'size': (150, 150), 'crop': 'smart'},
    },
}
SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}