SIMPLE DJANGO + REST FRAMEWORK + ANGULAR.JS

# Easy start Develop Environment
```bash
git clone https://Krukov@bitbucket.org/Krukov/simple-backlist.git backlist
cd backlist

virtualenv bin
source bin/activate
pip install -r requirements/local.txt
```
# Update (or initialize) database.
```bash
./manage.py syncdb
./manage.py migrate
```

##### Fake some data
```bash
./manage.py fakedata 30
```

##### Running development server

```bash
./manage.py runserver
```
